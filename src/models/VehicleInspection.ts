import Vehicle from "@/models/Vehicle";

export default interface VehicleInspection {
  vehicle: Vehicle;
  appointmentDate: Date;
  completionDate?: Date;
  isInspectionSuccessful?: boolean;
}

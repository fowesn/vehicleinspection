export default interface VehicleOwner {
  firstName: string;
  lastName: string;
  middleName?: string;
  phoneNumber: string;
  email?: string;
}

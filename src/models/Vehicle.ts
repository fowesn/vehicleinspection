import VehicleOwner from "@/models/VehicleOwner";

export default interface Vehicle {
  owner: VehicleOwner;
  number: string;
}

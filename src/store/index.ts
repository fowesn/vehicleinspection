import { createStore } from "vuex";
import VehicleInspection from "@/models/VehicleInspection";

export default createStore({
  state: {
    vehicleInspections: [] as Array<VehicleInspection>,
  },
  mutations: {
    setVehicleInspections(state) {
      const vehicleInspections = window.localStorage.getItem(
        "vehicle-inspection"
      );
      if (!vehicleInspections) return;
      state.vehicleInspections = JSON.parse(vehicleInspections).map(
        (inspection: VehicleInspection) => {
          inspection.appointmentDate = new Date(inspection.appointmentDate);
          if (inspection.completionDate)
            inspection.completionDate = new Date(inspection.completionDate);

          return inspection;
        }
      );
    },
    addVehicleInspection(state, vehicleInspection: VehicleInspection) {
      state.vehicleInspections.push(vehicleInspection);
      window.localStorage.setItem(
        "vehicle-inspection",
        JSON.stringify(state.vehicleInspections)
      );
    },
    removeVehicleInspection(state, vehicleInspection: VehicleInspection) {
      const index = state.vehicleInspections.findIndex(
        (inspection: VehicleInspection) => inspection === vehicleInspection
      );
      state.vehicleInspections.splice(index, 1);
      window.localStorage.setItem(
        "vehicle-inspection",
        JSON.stringify(state.vehicleInspections)
      );
    },
    completeVehicleInspectionSuccessfully(
      state,
      vehicleInspection: VehicleInspection
    ) {
      vehicleInspection.completionDate = new Date();
      vehicleInspection.isInspectionSuccessful = true;
      window.localStorage.setItem(
        "vehicle-inspection",
        JSON.stringify(state.vehicleInspections)
      );
    },
    completeVehicleInspectionUnsuccessfully(
      state,
      vehicleInspection: VehicleInspection
    ) {
      vehicleInspection.completionDate = new Date();
      vehicleInspection.isInspectionSuccessful = false;
      window.localStorage.setItem(
        "vehicle-inspection",
        JSON.stringify(state.vehicleInspections)
      );
    },
  },

  getters: {
    // список очереди на ТО, отсортированный по возрастанию назначенной даты
    activeVehicleInspections(state): Array<VehicleInspection> {
      return state.vehicleInspections
        .filter((inspection: VehicleInspection) => !inspection.completionDate)
        .sort(
          (x: VehicleInspection, y: VehicleInspection) =>
            x.appointmentDate.getTime() - y.appointmentDate.getTime()
        );
    },
    // список прошедших ТО, отсортированный по убыванию даты заверщения
    completedVehicleInspections(state): Array<VehicleInspection> {
      return state.vehicleInspections
        .filter((inspection: VehicleInspection) => inspection.completionDate)
        .sort(
          (x: VehicleInspection, y: VehicleInspection) =>
            <number>y.completionDate?.getTime() -
            <number>x.completionDate?.getTime()
        );
    },
  },
  actions: {},
  modules: {},
});

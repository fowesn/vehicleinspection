import { createRouter, createWebHashHistory, RouteRecordRaw } from "vue-router";
import Active from "../views/Active.vue";
import Completed from "../views/Completed.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "Active",
    component: Active,
  },
  {
    path: "/completed",
    name: "Completed",
    component: Completed,
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
